#!/bin/sh
# Mist
#
#   (C) Copyright 2010 Aymeric Mansoux
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.


UNISON="unison -ui text -auto -rsync"
MIST_VERSION="0.07"
MIST_DIR="mist"
CONF_DIR="${HOME}/.config/mist"

## Utils
make_config () 
{ 
    while [ "${REPLY_HUB}" != "Y" ]
    do
        read -p "[?] Enter the IP or name of the HUB: " HUB
        read -p "    HUB is '${HUB}', confirm (Y/N)? " REPLY_HUB
    done

    if [ -e "${CONF_DIR}/${HUB}.conf" ]
    then
        while [ "${REPLY_OVERWRITE}" != "Y" ]
        do
            if [ "${REPLY_OVERWRITE}" = "N" ]
            then
                echo "[*] Fine :)"
                exit 0
            fi
            read -p "[!] A config for '${HUB}' exists, overwrite? (Y/N) " REPLY_OVERWRITE
        done
    fi

    while [ "${REPLY_HUB_USER}" != "Y" ]
    do
        read -p "[?] Enter your username on the HUB: " HUB_USER
        read -p "    HUB username is '${HUB_USER}', confirm (Y/N)? " REPLY_HUB_USER
    done

    while [ "${REPLY_HUB_PORT}" != "Y" ]
    do
        read -p "[?] Enter the HUB ssh port number: " HUB_PORT
        read -p "    HUB ssh port number is '${HUB_PORT}', confirm (Y/N)? " REPLY_HUB_PORT
    done


    echo "HUB=\"${HUB}\"" > ~/.config/mist/${HUB}.conf
    echo "HUB_USER=\"${HUB_USER}\"" >> ~/.config/mist/${HUB}.conf
    echo "HUB_PORT=\"${HUB_PORT}\"" >> ~/.config/mist/${HUB}.conf
    
    # Create mist dirs if missing (ie, first time config)
    if [ ! -d "${MIST_DIR}" ]
    then
        mkdir -p ${MIST_DIR}
    fi
    if [ ! -d "${HOME}/.cache/mist" ]
    then
        mkdir -p ${HOME}/.cache/mist
    fi

    echo "[*] Thanks! Configuration done."
}

check_config () 
{
    if [ ! -d "${CONF_DIR}" ] && [ ! -s "${CONF_DIR}/*.conf" ]
    then
        echo "[!] This the first time that you are running mist."
        echo "    Please answer the following question to configure it."
        mkdir -p ${CONF_DIR}
        make_config
    elif [ "${CONFIG}" = 1 ]
    then
        make_config
    fi
}

## Basic tests
# Local Droplet test
check_local_droplet () 
{
    if [ $(/bin/ls ${HOME}/${MIST_DIR} | grep ${HUB} ) ]
    then
        echo "[*] Local ${HUB} droplet folder: OK"
        LOCAL_DROPLET=true
    else
        echo "[!] Local ${HUB} droplet folder not found"
        LOCAL_DROPLET=false
    fi
}

# Remote Droplet test
check_remote_droplet () 
{
    if [ $(ssh -p ${HUB_PORT} ${HUB_USER}@${HUB} /bin/ls | grep ${MIST_DIR}) ]
    then
        echo "[*] Remote ${HUB} droplet folder: OK"
        REMOTE_DROPLET=true
    else
        echo "[!] Remote ${HUB} droplet folder not found"
        REMOTE_DROPLET=false
    fi
}

# Be smart!
guess_situation () 
{
    if ( [ "${LOCAL_DROPLET}" = false ] && [ "${REMOTE_DROPLET}" = false ] )
    then
        echo "[ ] Creating local droplet"
        mkdir -p ${HOME}/${MIST_DIR}/${HUB}
        echo "[ ] Creating remote droplet"
        ssh -p ${HUB_PORT} ${HUB_USER}@${HUB} mkdir ${MIST_DIR}
    elif ( [ "${LOCAL_DROPLET}" = false ] && [ "${REMOTE_DROPLET}" = true ] )
    then
        echo "[ ] this machine is not part of your mist yet."
        echo "    fetching a copy of the remote droplet"
	if [ ! -d ~/.cache ]
        then
            mkdir ~/.cache
        fi
        rsync -P -a -e "ssh -p ${HUB_PORT}" ${HUB_USER}@${HUB}:${MIST_DIR} ~/.cache/
	mv ~/.cache/${MIST_DIR} ${HOME}/${MIST_DIR}/${HUB} 
    elif ( [ "${LOCAL_DROPLET}" = true ] && [ "${REMOTE_DROPLET}" = false ] )
    then
        echo "[!] I can see a local droplet folder, but there are no remote droplet."
        #FIXME gives possible reasons why
        echo "[!] Please fix."
        exit 1
    fi
}

# REMOTE online check
check_hub_online () 
{
    if [ $(ssh -p ${HUB_PORT} ${HUB_USER}@${HUB} pwd | grep home) ]
    then
        echo "[*] Remote HUB ${HUB}: OK"
        HUB_ONLINE=true
    else
        echo "[!] Remote HUB ${HUB}: Unreachable"
        exit 1
    fi
}

# Unison version check
check_unison_match () 
{
    UNISON_LOCAL=$(${UNISON} -version)
    UNISON_REMOTE=$(ssh -p ${HUB_PORT} ${HUB_USER}@${HUB} ${UNISON} -version)

    if [ "${UNISON_LOCAL}" = "${UNISON_REMOTE}" ]
    then
        echo "[*] Unison version match: OK"
    elif [ "${UNISON_REMOTE}" = "" ]
    then
        echo "[!] Unison not found on ${HUB}"
        echo "[!] Please contact your beloved ${HUB}'s root."
        break
    else
        echo "[!] Unison version match: Error"
        echo "    Local: ${UNISON_LOCAL}"
        echo "    Remote: ${UNISON_REMOTE}"
        echo "[!] Please contact your beloved ${HUB}'s root,"
        echo "[!] or fix your local unison version to match."
        exit 1
fi
}

## Finally do the sync
droplet_sync () 
{
    ${UNISON} ${HOME}/${MIST_DIR}/${HUB} ssh://${HUB_USER}@${HUB}:${HUB_PORT}/${MIST_DIR}
}

# And now the ponies do all the work
ponies ()
{
    check_config
    for CONF_FILE in $(/bin/ls ${CONF_DIR})
    do
        . ${CONF_DIR}/${CONF_FILE}
        check_hub_online
        check_unison_match
        check_local_droplet
        check_remote_droplet
        guess_situation
        droplet_sync
    done
}

daemon ()
{
    ponies
    inotifywait -m -r -e attrib -e modify -e move -e create -e delete  ${HOME}/${MIST_DIR} | while read SOMETHING
    do
        #echo ${CHANGES}
        ponies
    done
}

# Do as I say
while getopts nadhv ARG
do
	case $ARG in
	    n)	CONFIG=1 
            check_config 
            exit 0 ;;
	    a)  ponies
            exit 0 ;;
        h)	HUB="$OPTARG" ;;
        d)  daemon
            exit 0 ;;
        v)  echo ${MIST_VERSION}
            exit 0 ;;
	    ?)  echo "Usage: $0: [-n]"
		    exit 2;;
	esac
done

